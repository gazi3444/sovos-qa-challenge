﻿
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Amazon_Test_Project

{
    public class DriverUtils
    {

        public RemoteWebDriver GetDriver(string type)
        {
            RemoteWebDriver driver = null;
            switch (type)
            {
                case "chrome":

                    driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                    driver.Manage().Window.Maximize();
                    break;

                case "firefox":

                    driver = new FirefoxDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                    driver.Manage().Window.Maximize();
                    break;

                default:

                    driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                    driver.Manage().Window.Maximize();
                    break;

            }

            return driver;

        }

    }
}


