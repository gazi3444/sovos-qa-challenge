﻿using Newtonsoft.Json;
using System.IO;

namespace Amazon_Test_Project
{
    class DataUtils
    {
        public TestDataModel LoadData()
        {
            TestDataModel data = JsonConvert.DeserializeObject<TestDataModel>(File.ReadAllText("TestData.json"));
            return data;
        }
    }
}

