﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amazon_Test_Project
{
    class SearchObjectHelper
    {
        public readonly string searchTextBoxID = "twotabsearchtextbox";
        public readonly string searchButtonID = "nav-search-submit-text";
        public readonly string searchResultSelector = "#search > span > div > span > h1 > div > div.sg-col-14-of-20.sg-col-26-of-32.sg-col-18-of-24.sg-col.sg-col-22-of-28.s-breadcrumb.sg-col-10-of-16.sg-col-30-of-36.sg-col-6-of-12 > div > div > span.a-color-state.a-text-bold";
        public readonly string suggessionSelectorID = "issDiv6";
    }
}
