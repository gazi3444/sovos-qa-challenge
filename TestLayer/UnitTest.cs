using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using NUnit.Compatibility;


namespace Amazon_Test_Project
{
    [TestFixture]
    public class UnitTest
    {
        private RemoteWebDriver driver;
        private WebDriverWait wait;
        private TestDataModel data;
        private Common common;
        

        [SetUp]
        public void LoadTestData()
        {
            DataUtils dataUtils = new DataUtils();
            data = dataUtils.LoadData();
            common = new Common();
        }


        public void NavigateAmazon(string type)
        {
            DriverUtils driverUtils = new DriverUtils();
            driver = driverUtils.GetDriver(type);
            driver.Navigate().GoToUrl(Common.AmazonHomePageURL);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
        }

        public void searchAssert()
        {
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            wait.Until(drv => drv.FindElement(By.CssSelector(searchObjectHelper.searchResultSelector)));
            var searchResultText = driver.FindElement(By.CssSelector(searchObjectHelper.searchResultSelector)).GetAttribute("innerText");
            Assert.AreEqual( 0, string.Compare("\"" + data.searchText + "\"", searchResultText, true));
        }
        [Test]
        //Search product with Enter key
        public void SearchWithEnter()
        {
            NavigateAmazon(data.browser);
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            IWebElement searchBox = wait.Until(drv => drv.FindElement(By.Id(searchObjectHelper.searchTextBoxID)));
            searchBox.SendKeys(data.searchText + Keys.Enter);
            searchAssert();

        }

        [Test]
        //Search product  with Search Button
        public void SearchWithSearchButton()
        {
            NavigateAmazon(data.browser);
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            IWebElement searchBox = wait.Until(drv => drv.FindElement(By.Id(searchObjectHelper.searchTextBoxID)));
            searchBox.SendKeys(data.searchText);
            IWebElement searchbutton = driver.FindElement(By.Id(searchObjectHelper.searchButtonID));
            searchbutton.Click();
            searchAssert();
        }
        [Test]
        //Select product  from search sugessions
        public void SelectFromSugg()
        {
            NavigateAmazon(data.browser);
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            IWebElement searchBox = wait.Until(drv => drv.FindElement(By.Id(searchObjectHelper.searchTextBoxID)));
            searchBox.SendKeys(data.searchText);
            Thread.Sleep(2000);
            var selectsugg = driver.FindElement(By.Id(searchObjectHelper.suggessionSelectorID));
            selectsugg.Click();
            searchAssert();

        }
        [Test]
        //Check Auto correction for search results
        public void ResultAutoCorrection()
        {
            NavigateAmazon(data.browser);
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            IWebElement searchBox = wait.Until(drv => drv.FindElement(By.Id(searchObjectHelper.searchTextBoxID)));
            searchBox.SendKeys(data.wrongText + Keys.Enter);
            wait.Until(drv => drv.FindElement(By.CssSelector(searchObjectHelper.searchResultSelector)));
            var searchResultText = driver.FindElement(By.CssSelector(searchObjectHelper.searchResultSelector)).GetAttribute("innerText");
            Assert.AreEqual(0, string.Compare("\"" + data.autoCorrectedAs + "\"", searchResultText, true));

        }

        [Test]
        //Empty Search
        public void EmptySearch()
        {
            NavigateAmazon(data.browser);
            SearchObjectHelper searchObjectHelper = new SearchObjectHelper();
            IWebElement searchBox = wait.Until(drv => drv.FindElement(By.Id(searchObjectHelper.searchTextBoxID)));
            searchBox.Click();
            searchBox.SendKeys(Keys.Enter);
            string currentUrl = driver.Url;
            Assert.AreEqual(currentUrl, Common.AmazonHomePageURL);
          
        }


        [TearDown]
        public void Close()
        {
            driver.Close();
        }



    }
}
