﻿using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Amazon_Test_Project
{
    public class Common
    {

        public static readonly string AmazonHomePageURL = "https://www.amazon.com/";

        public string GetQueryParam(string url, string key)
        {
            Uri uri = new Uri(url);
            String value = HttpUtility.ParseQueryString(uri.Query).Get(key);
            return value;
        }
    }
}
